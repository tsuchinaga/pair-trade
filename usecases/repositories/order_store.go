package repositories

import "gitlab.com/tsuchinaga/pair-trade/domain/entities"

type OrderStore interface {
	GetActiveOrders() ([]*entities.Order, error)
	GetActivePairOrders() ([]*entities.PairOrder, error)
	GetOrderByCode(code string) (*entities.Order, error)
	GetPairOrderByCode(code string) (*entities.PairOrder, error)
	SavePairOrder(pairOrder *entities.PairOrder) error
}
