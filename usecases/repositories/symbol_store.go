package repositories

import "gitlab.com/tsuchinaga/pair-trade/domain/entities"

type SymbolStore interface {
	GetSymbol(code string) (*entities.Symbol, error)
	GetSymbols() ([]*entities.Symbol, error)
	SaveSymbol(symbol *entities.Symbol) error
}
