package repositories

import (
	"gitlab.com/tsuchinaga/pair-trade/domain/entities"
	"gitlab.com/tsuchinaga/pair-trade/domain/values"
)

type Security interface {
	Entry(order *entities.PairOrder) (*values.PairOrderResult, error)
	Exit(order *entities.PairOrder) (*values.PairOrderResult, error)
	Orders() ([]*values.SecurityOrder, error)
}
