package repositories

import "gitlab.com/tsuchinaga/pair-trade/domain/entities"

type PositionStore interface {
	GetPositions() ([]*entities.Position, error)
	GetPairPositions() ([]*entities.PairPosition, error)
	GetPositionByCode(code string) (*entities.Position, error)
	GetPairPositionByCode(code string) (*entities.PairPosition, error)
	SavePairPosition(pairPosition *entities.PairPosition) error
}
