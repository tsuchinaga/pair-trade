package entities

import (
	"errors"
	"reflect"
	"testing"

	"gitlab.com/tsuchinaga/pair-trade/domain/values"
)

func Test_PariSymbol_TradeQuantity(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name         string
		pair         *PairSymbol
		arg1, arg2   values.SymbolPrice
		arg3         float64
		want1, want2 *values.SymbolQuantity
		want3        error
	}{
		{name: "symbolAが初期化できていなかったらエラー",
			pair:  &PairSymbol{SymbolB: &Symbol{Code: "5678"}},
			want3: NotInitializedPairSymbolError},
		{name: "symbolBが初期化できていなかったらエラー",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234"}},
			want3: NotInitializedPairSymbolError},
		{name: "渡した銘柄価格Aの銘柄が存在しなければエラー",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234"}, SymbolB: &Symbol{Code: "5678"}},
			arg1:  values.SymbolPrice{SymbolCode: "1111", Price: 30000},
			arg2:  values.SymbolPrice{SymbolCode: "5678", Price: 30000},
			want3: NotMatchSymbolCodeError},
		{name: "渡した銘柄価格Bの銘柄が存在しなければエラー",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234"}, SymbolB: &Symbol{Code: "5678"}},
			arg1:  values.SymbolPrice{SymbolCode: "1234", Price: 30000},
			arg2:  values.SymbolPrice{SymbolCode: "5555", Price: 30000},
			want3: NotMatchSymbolCodeError},
		{name: "渡した銘柄価格AとBを足したときにすでに上限を越えていればエラー",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234", TradeUnit: 1}, SymbolB: &Symbol{Code: "5678", TradeUnit: 1}},
			arg1:  values.SymbolPrice{SymbolCode: "1234", Price: 30000},
			arg2:  values.SymbolPrice{SymbolCode: "5678", Price: 30000},
			arg3:  50000,
			want3: LowLimitPriceError},
		{name: "上限が10万で、1単元ずつ買えば5.1万と4.9万になるとき、1単元ずつの結果が返される",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234", TradeUnit: 1}, SymbolB: &Symbol{Code: "5678", TradeUnit: 1}},
			arg1:  values.SymbolPrice{SymbolCode: "1234", Price: 51_000},
			arg2:  values.SymbolPrice{SymbolCode: "5678", Price: 49_000},
			arg3:  100_000,
			want1: &values.SymbolQuantity{SymbolCode: "1234", Quantity: 1},
			want2: &values.SymbolQuantity{SymbolCode: "5678", Quantity: 1}},
		{name: "上限10万で、割と低価格、低単位でそれっぽい結果が返る",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234", TradeUnit: 1}, SymbolB: &Symbol{Code: "5678", TradeUnit: 1}},
			arg1:  values.SymbolPrice{SymbolCode: "1234", Price: 1_800},
			arg2:  values.SymbolPrice{SymbolCode: "5678", Price: 2_100},
			arg3:  100_000,
			want1: &values.SymbolQuantity{SymbolCode: "1234", Quantity: 27},
			want2: &values.SymbolQuantity{SymbolCode: "5678", Quantity: 23}},
		{name: "上限10万で、片方が高価格、片方が低価格でそれっぽい結果が返る",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234", TradeUnit: 1}, SymbolB: &Symbol{Code: "5678", TradeUnit: 1}},
			arg1:  values.SymbolPrice{SymbolCode: "1234", Price: 1_800},
			arg2:  values.SymbolPrice{SymbolCode: "5678", Price: 51_000},
			arg3:  100_000,
			want1: &values.SymbolQuantity{SymbolCode: "1234", Quantity: 27},
			want2: &values.SymbolQuantity{SymbolCode: "5678", Quantity: 1}},
		{name: "上限10万で、価格は同じでも取引単位が違う場合でそれっぽい結果が返る",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234", TradeUnit: 10}, SymbolB: &Symbol{Code: "5678", TradeUnit: 5}},
			arg1:  values.SymbolPrice{SymbolCode: "1234", Price: 1_750},
			arg2:  values.SymbolPrice{SymbolCode: "5678", Price: 1_750},
			arg3:  100_000,
			want1: &values.SymbolQuantity{SymbolCode: "1234", Quantity: 20},
			want2: &values.SymbolQuantity{SymbolCode: "5678", Quantity: 20}},
		{name: "上限10万で、価格が違い取引単位が違う場合でそれっぽい結果が返る",
			pair:  &PairSymbol{SymbolA: &Symbol{Code: "1234", TradeUnit: 10}, SymbolB: &Symbol{Code: "5678", TradeUnit: 5}},
			arg1:  values.SymbolPrice{SymbolCode: "1234", Price: 1_950},
			arg2:  values.SymbolPrice{SymbolCode: "5678", Price: 1_750},
			arg3:  100_000,
			want1: &values.SymbolQuantity{SymbolCode: "1234", Quantity: 20},
			want2: &values.SymbolQuantity{SymbolCode: "5678", Quantity: 30}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			got1, got2, got3 := test.pair.TradeQuantity(test.arg1, test.arg2, test.arg3)
			if !reflect.DeepEqual(test.want1, got1) || !reflect.DeepEqual(test.want2, got2) || !errors.Is(got3, test.want3) {
				t.Errorf("%s error\nwant: %+v, %+v, %+v\ngot: %+v, %+v, %+v\n", t.Name(),
					test.want1, test.want2, test.want3, got1, got2, got3)
			}
		})
	}
}
