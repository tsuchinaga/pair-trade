package entities

import (
	"errors"
	"math"

	"gitlab.com/tsuchinaga/pair-trade/domain/values"
)

var (
	NotInitializedPairSymbolError = errors.New("pair symbol is not initialized") // 銘柄ペアが初期化されていない
	NotMatchSymbolCodeError       = errors.New("not match symbol codes")         // 銘柄コードの指定が合わない
	LowLimitPriceError            = errors.New("limit price is too low")         // 上限が低いために売買できない
)

// PairSymbol - 銘柄ペア
type PairSymbol struct {
	SymbolA *Symbol
	SymbolB *Symbol
}

// TradeQuantity - 各銘柄の値段から売買する数量を決定する
//   priceAとpriceBは順不同
//   ただし、symbolA, BとpriceA, Bのセットが噛み合ってないとエラー
//   またlimitが低すぎるなどで売買できない場合もエラー
func (p *PairSymbol) TradeQuantity(priceA values.SymbolPrice, priceB values.SymbolPrice, limitPrice float64) (*values.SymbolQuantity, *values.SymbolQuantity, error) {
	if p.SymbolA == nil || p.SymbolB == nil {
		return nil, nil, NotInitializedPairSymbolError
	}

	var symbolAPrice, symbolBPrice float64
	if p.SymbolA.Code == priceA.SymbolCode && p.SymbolB.Code == priceB.SymbolCode {
		symbolAPrice, symbolBPrice = priceA.Price, priceB.Price
	} else if p.SymbolB.Code == priceA.SymbolCode && p.SymbolA.Code == priceB.SymbolCode {
		symbolBPrice, symbolAPrice = priceA.Price, priceB.Price
	} else {
		return nil, nil, NotMatchSymbolCodeError
	}

	var qA, qB float64
	pA, pB := symbolAPrice*p.SymbolA.TradeUnit, symbolBPrice*p.SymbolB.TradeUnit
	if pA+pB > limitPrice {
		return nil, nil, LowLimitPriceError
	}

	// limitPrice/2 に一番近い価格にする
	{
		m := math.Round((limitPrice / 2) / pA)
		if m > 0 {
			qA = m * p.SymbolA.TradeUnit
			pA = symbolAPrice * qA
		}
	}
	{
		m := math.Round((limitPrice / 2) / pB)
		if m > 0 {
			qB = m * p.SymbolB.TradeUnit
			pB = symbolBPrice * qB
		}
	}

	// 上限を下回れるまで、高いほうから1単元ずつ減らしていく
	for limitPrice < (pA + pB) {
		if (p.SymbolA.TradeUnit != qA && pA > pB) || p.SymbolB.TradeUnit == qB { // Aが減らせてAのほうが高いか、Bが減らせない
			qA -= p.SymbolA.TradeUnit
			pA = symbolAPrice * qA
		} else if (p.SymbolB.TradeUnit != qB && pB > pA) || p.SymbolA.TradeUnit == qA { // Bが減らせてBのほうが高いか、Aが減らせない
			qB -= p.SymbolA.TradeUnit
			pA = symbolBPrice * qB
		} else { // 上記以外の場合は両方から減らす
			qA -= p.SymbolA.TradeUnit
			pA = symbolAPrice * qA
			qB -= p.SymbolA.TradeUnit
			pA = symbolBPrice * qB
		}
	}

	return &values.SymbolQuantity{SymbolCode: p.SymbolA.Code, Quantity: qA}, &values.SymbolQuantity{SymbolCode: p.SymbolB.Code, Quantity: qB}, nil
}
