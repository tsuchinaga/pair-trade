package entities

import (
	"reflect"
	"testing"

	"gitlab.com/tsuchinaga/pair-trade/domain/values"
)

func Test_PairOrder_Status(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name string
		pair *PairOrder
		want values.OrderStatus
	}{
		{name: "newで一致していればnew", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusNew}, OrderB: &Order{Status: values.OrderStatusNew}}, want: values.OrderStatusNew},
		{name: "inOrderで一致していればinOrder", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusInOrder}, OrderB: &Order{Status: values.OrderStatusInOrder}}, want: values.OrderStatusInOrder},
		{name: "partで一致していればpart", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusPart}, OrderB: &Order{Status: values.OrderStatusPart}}, want: values.OrderStatusPart},
		{name: "doneで一致していればdone", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusDone}, OrderB: &Order{Status: values.OrderStatusDone}}, want: values.OrderStatusDone},
		{name: "inCancelで一致していればinCancel", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusInCancel}, OrderB: &Order{Status: values.OrderStatusInCancel}}, want: values.OrderStatusInCancel},
		{name: "canceledで一致していればcanceled", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusCanceled}, OrderB: &Order{Status: values.OrderStatusCanceled}}, want: values.OrderStatusCanceled},
		{name: "errorで一致していればerror", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusError}, OrderB: &Order{Status: values.OrderStatusError}}, want: values.OrderStatusError},
		{name: "片方がerrorならerror", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusDone}, OrderB: &Order{Status: values.OrderStatusError}}, want: values.OrderStatusError},
		{name: "片方がinCancelならinCancel", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusInCancel}, OrderB: &Order{Status: values.OrderStatusCanceled}}, want: values.OrderStatusInCancel},
		{name: "片方が注文中の状態ならpart", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusDone}, OrderB: &Order{Status: values.OrderStatusInOrder}}, want: values.OrderStatusPart},
		{name: "上記以外ならerror", pair: &PairOrder{OrderA: &Order{Status: values.OrderStatusDone}, OrderB: &Order{Status: values.OrderStatusCanceled}}, want: values.OrderStatusError},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			got := test.pair.Status()
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}
