package entities

// Symbol - 銘柄
type Symbol struct {
	Code       string  // 銘柄コード
	Name       string  // 名前
	TradeUnit  float64 // 取引単位
	UpperLimit float64 // 値幅上限
	LowerLimit float64 // 値幅下限
	MarginBuy  bool    // 信用買いができるか
	MarginSell bool    // 信用売りができるか
}
