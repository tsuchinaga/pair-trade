package entities

import (
	"time"

	"gitlab.com/tsuchinaga/pair-trade/domain/values"
)

// Order - 注文
// TODO 同時に変更されうるならmutexをつけることを検討する
type Order struct {
	Code               string             // 証券会社によってつけられた注文のコード
	SymbolCode         string             // 銘柄コード
	TradeType          values.TradeType   // 取引種別
	Side               values.Side        // 売買方向
	Status             values.OrderStatus // 注文の状態
	Quantity           float64            // 数量
	ContractedQuantity float64            // 約定済み数量
	ErrorMessage       string             // エラー時の理由
	OrderTime          time.Time          // 注文日時
	Contracts          []*Contract        // 約定詳細
	ExitPositionList   []*ExitPosition    // 返済ポジションリスト
}

// Contract - 約定
type Contract struct {
	PositionCode string
	Quantity     float64
	Price        float64
	ContractTime time.Time
}

// ExitPosition - エグジットするポジション
type ExitPosition struct {
	PositionCode string
	Quantity     float64
}

// SetSecurityCode - 証券会社によってつけられた注文コードを追加し、同時に注文中状態に更新する
func (o *Order) SetSecurityCode(securityCode string) {
	o.Code = securityCode
	o.Status = values.OrderStatusInOrder
}

// IsContainsPositionCode - ポジションコードが含まれているか
func (o *Order) IsContainsPositionCode(positionCode string) bool {
	if o.Contracts == nil {
		o.Contracts = []*Contract{}
	}

	for _, contract := range o.Contracts {
		if positionCode == contract.PositionCode {
			return true
		}
	}
	return false
}

// Contract - 約定数量を追加し、同時に注文の状態を更新する
func (o *Order) Contract(contract *Contract) {
	// 約定が反映済みなら何もせず終了
	// 不自然な約定数なら何もしない
	if o.IsContainsPositionCode(contract.PositionCode) || o.Quantity-o.ContractedQuantity < contract.Quantity || contract.Quantity <= 0 {
		return
	}

	o.ContractedQuantity += contract.Quantity
	o.Contracts = append(o.Contracts, contract)

	switch {
	case o.Quantity == o.ContractedQuantity:
		o.Status = values.OrderStatusDone
	case o.ContractedQuantity > 0:
		o.Status = values.OrderStatusPart
	default:
		o.Status = values.OrderStatusInOrder
	}
}

// SetError - エラーメッセージを追加し、エラー状態にする
func (o *Order) SetError(message string) {
	o.ErrorMessage = message
	o.Status = values.OrderStatusError
}
