package entities

import (
	"reflect"
	"testing"
)

func Test_Position_IsOwned(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name     string
		position *Position
		want     bool
	}{
		{name: "OwnedQuantityが1以上ならtrue", position: &Position{OwnedQuantity: 1}, want: true},
		{name: "OwnedQuantityもHoldQuantityも1以上ならtrue", position: &Position{OwnedQuantity: 1, HoldQuantity: 1}, want: true},
		{name: "OwnedQuantityもHoldQuantityも0ならfalse", position: &Position{}, want: false},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			got := test.position.IsOwned()
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_Position_Contract(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name     string
		position *Position
		arg      float64
		want     *Position
	}{
		{name: "引数が0未満なら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: -1,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "引数が0なら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 0,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "拘束数より多い約定数がきたら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 4,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "拘束数と同じ約定数がきたら保有数拘束数を減らす", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 3,
			want: &Position{OwnedQuantity: 2, HoldQuantity: 0}},
		{name: "拘束数より少ない約定数がきたら保有数拘束数を減らす", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 2,
			want: &Position{OwnedQuantity: 3, HoldQuantity: 1}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.position.Contract(test.arg)
			if !reflect.DeepEqual(test.want, test.position) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, test.position.HoldQuantity)
			}
		})
	}
}

func Test_Position_Hold(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name     string
		position *Position
		arg      float64
		want     *Position
	}{
		{name: "引数が0未満なら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: -1,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "引数が0なら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 0,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "保有数-拘束数より多い拘束数がきたら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 3,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "保有数-拘束数と同じ拘束数がきたら拘束数を増やす", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 2,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 5}},
		{name: "保有数-拘束数より少ない拘束数がきたら拘束数を増やす", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 1,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 4}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.position.Hold(test.arg)
			if !reflect.DeepEqual(test.want, test.position) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, test.position)
			}
		})
	}
}

func Test_Position_Release(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name     string
		position *Position
		arg      float64
		want     *Position
	}{
		{name: "引数が0未満なら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: -1,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "引数が0なら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 0,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "拘束数より多い解放数がきたら何もしない", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 4,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 3}},
		{name: "拘束数と同じ解放数がきたら拘束数を減らす", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 3,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 0}},
		{name: "拘束数より少ない解放数がきたら拘束数を減らす", position: &Position{OwnedQuantity: 5, HoldQuantity: 3}, arg: 2,
			want: &Position{OwnedQuantity: 5, HoldQuantity: 1}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.position.Release(test.arg)
			if !reflect.DeepEqual(test.want, test.position) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, test.position)
			}
		})
	}
}
