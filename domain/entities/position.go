package entities

import "time"

// Position - 建玉・ポジション
// TODO 同時に変更されうるならmutexをつけることを検討する
type Position struct {
	Code           string    // 証券会社によってつけられたポジションのコード
	OrderCode      string    // ポジションを獲得した注文のコード
	SymbolCode     string    // 銘柄コード
	Price          float64   // 約定値
	Quantity       float64   // ポジション獲得時の数量
	OwnedQuantity  float64   // 保有数
	HoldQuantity   float64   // 拘束数
	ContractedTime time.Time // ポジション獲得時の日時
}

// IsOwned - 保有中か
func (p *Position) IsOwned() bool {
	return p.OwnedQuantity > 0
}

// Contract - 拘束していたポジションが約定したのでHoldから減らす
func (p *Position) Contract(quantity float64) {
	if p.HoldQuantity < quantity || quantity <= 0 {
		return
	}
	p.OwnedQuantity -= quantity
	p.HoldQuantity -= quantity
}

// Hold - ポジションを拘束する
func (p *Position) Hold(quantity float64) {
	if p.HoldQuantity+quantity > p.OwnedQuantity || quantity <= 0 {
		return
	}
	p.HoldQuantity += quantity
}

// Release - 拘束していたポジションを開放する
func (p *Position) Release(quantity float64) {
	if p.HoldQuantity < quantity || quantity <= 0 {
		return
	}
	p.HoldQuantity -= quantity
}
