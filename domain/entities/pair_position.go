package entities

type PairPosition struct {
	PositionA *Position
	PositionB *Position
}
