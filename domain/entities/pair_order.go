package entities

import "gitlab.com/tsuchinaga/pair-trade/domain/values"

type PairOrder struct {
	OrderA *Order
	OrderB *Order
}

func (p *PairOrder) Status() values.OrderStatus {
	switch {
	case p.OrderA.Status == p.OrderB.Status: // 両方が一致している場合はそれがステータス
		return p.OrderA.Status
	case p.OrderA.Status == values.OrderStatusError || p.OrderB.Status == values.OrderStatusError: // どちらかがerrorならerror
		return values.OrderStatusError
	case p.OrderA.Status == values.OrderStatusInCancel || p.OrderB.Status == values.OrderStatusInCancel: // どちらかが取消中なら取消中
		return values.OrderStatusInCancel
	case p.OrderA.Status == values.OrderStatusInOrder || p.OrderA.Status == values.OrderStatusPart ||
		p.OrderB.Status == values.OrderStatusInOrder || p.OrderB.Status == values.OrderStatusPart: // どちらかが全約定していなければ部分約定
		return values.OrderStatusPart
	default: // 上記にマッチしなければerror
		return values.OrderStatusError
	}
}
