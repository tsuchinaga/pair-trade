package entities

import (
	"reflect"
	"testing"

	"gitlab.com/tsuchinaga/pair-trade/domain/values"
)

func Test_Order_SetError(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		order *Order
		arg   string
		want  *Order
	}{
		{name: "メッセージを追加し、状態がerrorに更新される",
			order: &Order{}, arg: "error message", want: &Order{Status: values.OrderStatusError, ErrorMessage: "error message"}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.order.SetError(test.arg)
			if !reflect.DeepEqual(test.want, test.order) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, test.order)
			}
		})
	}
}

func Test_Order_SetSecurityCode(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		order *Order
		arg   string
		want  *Order
	}{
		{name: "コードを追加し、状態がinOrderに更新される",
			order: &Order{}, arg: "SEC-CODE", want: &Order{Code: "SEC-CODE", Status: values.OrderStatusInOrder}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.order.SetSecurityCode(test.arg)
			if !reflect.DeepEqual(test.want, test.order) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, test.order)
			}
		})
	}
}

func Test_Order_IsContainsPositionID(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		order *Order
		arg   string
		want  bool
	}{
		{name: "nilなら空配列を作ってfalseを返す", order: &Order{}, arg: "POS-CODE", want: false},
		{name: "空配列ならfalse", order: &Order{Contracts: []*Contract{}}, arg: "POS-CODE", want: false},
		{name: "配列の中に引数がなかったらfalse", order: &Order{Contracts: []*Contract{{PositionCode: "FOO"}, {PositionCode: "BAR"}, {PositionCode: "BAZ"}}}, arg: "POS-CODE", want: false},
		{name: "配列の中に引数があったらtrue", order: &Order{Contracts: []*Contract{{PositionCode: "FOO"}, {PositionCode: "POS-CODE"}, {PositionCode: "BAR"}, {PositionCode: "BAZ"}}}, arg: "POS-CODE", want: true},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			got := test.order.IsContainsPositionCode(test.arg)
			if !reflect.DeepEqual(test.want, got) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, got)
			}
		})
	}
}

func Test_Order_Contract(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		order *Order
		arg   *Contract
		want  *Order
	}{
		{name: "すでに登録されたポジションなら何もしない",
			order: &Order{Contracts: []*Contract{{PositionCode: "POS-CODE"}}},
			arg:   &Contract{PositionCode: "POS-CODE"},
			want:  &Order{Contracts: []*Contract{{PositionCode: "POS-CODE"}}}},
		{name: "注文残数より多いquantityなら何もしない",
			order: &Order{Quantity: 3, ContractedQuantity: 3},
			arg:   &Contract{PositionCode: "POS-CODE", Quantity: 1},
			want:  &Order{Quantity: 3, ContractedQuantity: 3, Contracts: []*Contract{}}},
		{name: "quantityが0以下なら何もしない",
			order: &Order{Quantity: 3, ContractedQuantity: 1},
			arg:   &Contract{PositionCode: "POS-CODE", Quantity: 0},
			want:  &Order{Quantity: 3, ContractedQuantity: 1, Contracts: []*Contract{}}},
		{name: "約定数が0ならstatusはinOrderになる",
			order: &Order{Quantity: 3, ContractedQuantity: -1},
			arg:   &Contract{PositionCode: "POS-CODE", Quantity: 1},
			want:  &Order{Quantity: 3, ContractedQuantity: 0, Status: values.OrderStatusInOrder, Contracts: []*Contract{{PositionCode: "POS-CODE", Quantity: 1}}}},
		{name: "約定数が1以上で注文残数が1以上ならstatusはpartになる",
			order: &Order{Quantity: 3, ContractedQuantity: 1},
			arg:   &Contract{PositionCode: "POS-CODE", Quantity: 1},
			want:  &Order{Quantity: 3, ContractedQuantity: 2, Status: values.OrderStatusPart, Contracts: []*Contract{{PositionCode: "POS-CODE", Quantity: 1}}}},
		{name: "注文残数がなければstatusはdoneになる",
			order: &Order{Quantity: 3, ContractedQuantity: 1},
			arg:   &Contract{PositionCode: "POS-CODE", Quantity: 2},
			want:  &Order{Quantity: 3, ContractedQuantity: 3, Status: values.OrderStatusDone, Contracts: []*Contract{{PositionCode: "POS-CODE", Quantity: 2}}}},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.order.Contract(test.arg)
			if !reflect.DeepEqual(test.want, test.order) {
				t.Errorf("%s error\nwant: %+v\ngot: %+v\n", t.Name(), test.want, test.order)
			}
		})
	}
}
