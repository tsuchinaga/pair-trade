package values

// SymbolQuantity - 銘柄の株数
type SymbolQuantity struct {
	SymbolCode string
	Quantity   float64
}
