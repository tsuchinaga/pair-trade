package values

// Side - 売買方向
type Side string

const (
	SideUnspecified Side = ""
	SideBuy         Side = "buy"
	SideSell        Side = "sell"
)
