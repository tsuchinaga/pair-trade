package values

// OrderStatus - 注文状態
type OrderStatus string

const (
	OrderStatusUnspecified OrderStatus = ""
	OrderStatusNew         OrderStatus = "new"
	OrderStatusInOrder     OrderStatus = "inOrder"
	OrderStatusPart        OrderStatus = "part"
	OrderStatusDone        OrderStatus = "done"
	OrderStatusInCancel    OrderStatus = "inCancel"
	OrderStatusCanceled    OrderStatus = "canceled"
	OrderStatusError       OrderStatus = "error"
)
