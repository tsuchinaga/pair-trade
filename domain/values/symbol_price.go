package values

// SymbolPrice - 銘柄の価格
type SymbolPrice struct {
	SymbolCode string
	Price      float64
}
