package values

type PairOrderResult struct {
	EntryResultA *OrderResult
	EntryResultB *OrderResult
}

type OrderResult struct {
	Result       bool
	Code         string
	ErrorMessage string
}
