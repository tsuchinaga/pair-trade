package values

type TradeType string

const (
	TradeTypeUnspecified TradeType = ""
	TradeTypeEntry       TradeType = "entry"
	TradeTypeExit        TradeType = "exit"
)
