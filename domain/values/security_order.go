package values

import "time"

type SecurityOrder struct {
	Code               string
	Status             OrderStatus
	Quantity           float64
	ContractedQuantity float64
	Contracts          []*SecurityContract
}

type SecurityContract struct {
	PositionCode string
	Quantity     float64
	Price        float64
	ContractTime time.Time
}
